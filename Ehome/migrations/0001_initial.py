# Generated by Django 3.0.1 on 2020-01-28 19:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('menu_name', models.CharField(max_length=50)),
                ('menu_link', models.CharField(default=None, max_length=500)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'menu',
            },
        ),
        migrations.CreateModel(
            name='Submenu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('submenu_title', models.CharField(default=None, max_length=100)),
                ('submenu_name', models.CharField(max_length=100)),
                ('submenu_link', models.CharField(max_length=500)),
                ('is_second_sub', models.BooleanField(default=False)),
                ('second_sub_under', models.CharField(default=None, max_length=100)),
                ('active', models.BooleanField(default=True)),
                ('submenu_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Ehome.Menu')),
            ],
            options={
                'db_table': 'submenu',
            },
        ),
    ]
