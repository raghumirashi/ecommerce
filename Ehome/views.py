from django.shortcuts import render, get_object_or_404
from .models import Menu, Submenu, BackgroundImages
from collections import Counter
from django.http import Http404


def index(request):
    try:
        menu = Menu.objects.all()
        submenu = Submenu.objects.all()
        slider_images = BackgroundImages.objects.filter(set_to_slider=True)
        # product = Products.objects.all()
        value = []
        for sub in submenu:
            if sub.submenu_title != '':
                value.append(sub.submenu_title)
        values1 = dict(Counter(value))
    except slider_images.DoesNotExist:
        raise Http404('Slider image not exists')
    else:
        return render(request, 'index.html', {'menu': menu, 'submenu': submenu, 'values1': values1,
                                              'slider_images': slider_images})
