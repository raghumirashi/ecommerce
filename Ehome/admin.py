from django.contrib import admin
from .models import Menu, Submenu, BackgroundImages
# from django.db import models
# from django.contrib.auth.forms import forms


class MenuAdmin(admin.ModelAdmin):
    list_display = ('menu_name', 'menu_link', 'active', 'menu_order', 'has_submenu')


class SubmenuAdmin(admin.ModelAdmin):
    list_display = ('submenu_title', 'submenu_name', 'submenu_link', 'is_second_sub', 'second_sub_under', 'active')
    list_display_links = ('submenu_title', 'submenu_name')  # this is for clickable


class BackgroundImagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'set_to_slider', 'primary_image', 'set_to_background')


admin.site.register(Menu, MenuAdmin)
admin.site.register(Submenu, SubmenuAdmin)
admin.site.register(BackgroundImages, BackgroundImagesAdmin)
