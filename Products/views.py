from django.shortcuts import render, redirect
from django.contrib import messages
from math import ceil
from .models import Menu, Submenu, BackgroundImages, Subscribers, Products, Categories
from collections import Counter
from django.http import Http404
from django.db import IntegrityError


def header_data():
    try:
        menu = Menu.objects.all()
        submenu = Submenu.objects.all()
        slider_images = BackgroundImages.objects.filter(set_to_slider=True)
        value = []
        for sub in submenu:
            if sub.submenu_title != '':
                value.append(sub.submenu_title)
        values1 = dict(Counter(value))
    except slider_images.DoesNotExist:
        raise Http404('Slider image not exists')
    else:
        return {'menu': menu, 'submenu': submenu, 'values1': values1, 'slider_images': slider_images}


def all_cat_product():
    cat_product = Products.objects.values('category_id', 'id')
    all_products = []
    cats = {item['category_name'] for item in cat_product}
    for cat in cats:
        prod = Products.objects.filter(category_id=cat)
        n = len(prod)
        slides = n // 4 + ceil((n / 4) - (n // 4))
        all_products.append([prod, range(1, slides), slides, cats])
    print(all_products)
    return cat_product


def index(request):
    head_nav = header_data()
    all_cats = Categories.objects.all()
    rate = {1: 2, 2: 4, 3: 4, 4: 3, 5: 3, 6: 4, 7: 0, 8: 5, }
    rates = {}
    r = {0: '00000', 1: '10000', 2: '11000', 3: '11100', 4: '11110', 5: '11111'}
    for rr in rate:
        if rate[rr] in r:
            rates[rr] = r[rate[rr]]

    all_products = []

    prod = Products.objects.all()
    n = len(prod)
    slides = n // 4 + ceil((n / 4) - (n // 4))
    all_rates = []

    all_products.append([prod, range(1, slides), slides, all_rates])
    param = {'all_products': all_products, 'head_nav': head_nav, 'all_cats': all_cats, 'prod': prod}
    # print(param['all_products'][0])
    return render(request, 'index.html', param)


# def product():
# catproduct = Products.objects.values('category_id', 'id')
# all_products = []
# cats = {item['category_id'] for item in catproduct}
# for cat in cats:
#     prod = Products.objects.filter(category_id=cat)
#     n = len(prod)
#     slides = n // 4 + ceil((n / 4) - (n // 4))
#     all_rates = []
#     product_list = {item['id'] for item in catproduct if item['category_id'] == cat}
#
#     for product in product_list:
#         if product in rates:
#             # all_rates[product] = rates[product]
#             all_rates.append(rates[product])
#
#     all_products.append([prod, range(1, slides), slides, all_rates])
# # print(all_products)
# param = {'all_products': all_products, 'menu': menu, 'submenu': submenu, 'values1': values1,
#          'slider_images': slider_images}
# return render(request, 'index.html', param)

def subscribe(request):
    if request.method == 'GET':
        try:
            post = Subscribers()
            post.email = request.GET['email']
            post.save()
            messages.info(request, 'You are subscribed Successfully. Thank You!')
        except IntegrityError as e:
            messages.info(request, 'Please provide valid email. Thank You!')
            return redirect('index')
        return redirect('index')
    else:
        return redirect('index')
