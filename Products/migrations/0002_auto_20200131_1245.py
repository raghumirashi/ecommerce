# Generated by Django 3.0.1 on 2020-01-31 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Products', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='offer',
            field=models.CharField(max_length=100),
        ),
    ]
