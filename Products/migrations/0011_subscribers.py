# Generated by Django 3.0.1 on 2020-02-09 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Products', '0010_backgroundimages_menu_submenu'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscribers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('subscribe_date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'subscribers',
            },
        ),
    ]
