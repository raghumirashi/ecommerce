from django.db import models


'''Below code for dynamic navigation bar ---Code starts here---'''


class Menu(models.Model):
    """This below code is for Navigation menu"""
    menu_name = models.CharField(max_length=50)
    menu_link = models.CharField(max_length=500, default=None)
    active = models.BooleanField(default=True)
    has_submenu = models.BooleanField(default=True)
    menu_order = models.PositiveIntegerField(blank=False, null=False, unique=True)

    class Meta(object):
        db_table = 'menu'
        ordering = ['menu_order']

    def __str__(self):
        return self.menu_name


class Submenu(models.Model):
    """this is for submenu items for above navigation submenu"""
    submenu_id = models.ForeignKey(Menu, on_delete=models.CASCADE)
    submenu_title = models.CharField(max_length=100, default=None, blank=True)
    submenu_name = models.CharField(max_length=100)
    submenu_link = models.CharField(max_length=500)
    is_second_sub = models.BooleanField(default=False)  # this is for second sub menu
    second_sub_under = models.CharField(max_length=100, blank=True)  # this is for second submenu under which it comes
    active = models.BooleanField(default=True)

    class Meta:
        db_table = 'submenu'

    def __str__(self):
        return self.submenu_name


class BackgroundImages(models.Model):
    """this is for slider and other background images"""
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='background')
    set_to_slider = models.BooleanField(default=False)
    primary_image = models.BooleanField(default=False)
    set_to_background = models.BooleanField(default=False)

    class Meta:
        db_table = 'backgroundImages'

    def __str__(self):
        return self.name


'''Navigation code Ends here'''


class Subscribers(models.Model):
    email = models.EmailField(null=False, unique=True)
    subscribe_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'subscribers'

    def __self__(self):
        return self.email


class Categories(models.Model):
    category_name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.category_name


class SubCategories(models.Model):
    category_id = models.ForeignKey(Categories, on_delete=models.CASCADE)
    sub_category_name = models.CharField(max_length=200)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sub_category_name


class Products(models.Model):
    category_id = models.ForeignKey(Categories, on_delete=models.CASCADE)
    sub_category_id = models.ForeignKey(SubCategories, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=200)
    product_sku = models.CharField(max_length=200, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    weight = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    short_description = models.TextField(blank=True)
    long_description = models.TextField(blank=True)
    offer = models.CharField(max_length=100, blank=True)
    quantity = models.IntegerField()
    image = models.ImageField(upload_to='products', blank=True)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'products'

    def __str__(self):
        return self.product_name

