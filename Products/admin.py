from django.contrib import admin
from .models import Products, Categories, SubCategories
from .models import Menu, Submenu, BackgroundImages, Subscribers


'''Below code for navigation bar Starts here'''


class MenuAdmin(admin.ModelAdmin):
    list_display = ('menu_name', 'menu_link', 'active', 'menu_order', 'has_submenu')


class SubmenuAdmin(admin.ModelAdmin):
    list_display = ('submenu_title', 'submenu_name', 'submenu_link', 'is_second_sub', 'second_sub_under', 'active')
    list_display_links = ('submenu_title', 'submenu_name')  # this is for clickable


class BackgroundImagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'set_to_slider', 'primary_image', 'set_to_background')


admin.site.register(Menu, MenuAdmin)
admin.site.register(Submenu, SubmenuAdmin)
admin.site.register(BackgroundImages, BackgroundImagesAdmin)

'''Navigation code ends here'''


class SubscribersAdmin(admin.ModelAdmin):
    list_display = ('email', 'subscribe_date')
    readonly_fields = ('email', 'subscribe_date')
    list_display_links = None


admin.site.register(Subscribers, SubscribersAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ('category_id', 'sub_category_id', 'product_name', 'product_sku', 'price', 'weight', 'short_description',
                    'long_description', 'image', 'offer', 'quantity', 'created_on')
    list_display_links = ('product_name', )


class SubCategoriesAdmin(admin.ModelAdmin):
    list_display = ('category_id', 'sub_category_name', 'description', 'created_on')
    list_display_links = ('sub_category_name',)


class CategoriesAdmin(admin.ModelAdmin):
    list_display = ('category_name', 'created_on')


admin.site.register(Products, ProductAdmin)
admin.site.register(Categories, CategoriesAdmin)
admin.site.register(SubCategories, SubCategoriesAdmin)
